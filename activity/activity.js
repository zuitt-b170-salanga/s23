/* s23-activity.js */

// Insert a single room using the insertOne method
db.rooms.insertOne(
{
	"name":"single",
	"accomodates": 2,
	"price": 1000,
	"description":"A simple room with all the basic necesities",
	"rooms_available": 10,
	"isAvailable": false
}
);


// Insert multiple rooms using the insertMany method
db.rooms.insertMany([
{
	"name":"double",
	"accomodates": 3,
	"price": 2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available": 5,
	"isAvailable": false
},
	
{
	"name":"queen",
	"accomodates": 4,
	"price": 4000,
	"description":"A room with a queen-sized bed perfect for a simple getaway",
	"rooms_available": 15,
	"isAvailable": false
}
]);


// Use the find method to search for a room with the name double
db.rooms.find({ name: "double" });


// Use the updateOne method to update the queen room & set the available rooms to 0
db.rooms.updateOne(
    {
        name: "queen"
    },
    {
        $set: {
            rooms_available: 0
        }
    }
)


// Use the deleteMany method to delete all rooms with 0 availability
db.rooms.deleteMany({rooms_available: 0})

